import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogCrearDetalleComponent } from './dialog-crear-detalle.component';

describe('DialogCrearDetalleComponent', () => {
  let component: DialogCrearDetalleComponent;
  let fixture: ComponentFixture<DialogCrearDetalleComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DialogCrearDetalleComponent]
    });
    fixture = TestBed.createComponent(DialogCrearDetalleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
