//import { Component } from '@angular/core';

import {Component, Inject} from '@angular/core';
import {MatDialog, MAT_DIALOG_DATA, MatDialogRef, MatDialogModule} from '@angular/material/dialog';
import {NgIf} from '@angular/common';
import {MatButtonModule} from '@angular/material/button';
import {FormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import { ConsumoComponent } from '../consumo/consumo.component';

export interface DialogData {
  animal: string;
  name: string;
}

@Component({
  selector: 'app-dialog-crear-detalle',
  templateUrl: './dialog-crear-detalle.component.html',
  styleUrls: ['./dialog-crear-detalle.component.css'],
  standalone: true,
  imports: [MatDialogModule, MatFormFieldModule, MatInputModule, FormsModule, MatButtonModule]
})
export class DialogCrearDetalleComponent {

  constructor(
    public dialogRef: MatDialogRef<ConsumoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

}
