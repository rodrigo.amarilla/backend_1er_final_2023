
import { Component, OnInit } from '@angular/core';


import { ReservaService } from '../../services/reserva.service';

@Component({
  selector: 'app-reserva',
  templateUrl: './reserva.component.html',
  styleUrls: ['./reserva.component.css']
})
export class ReservaComponent implements OnInit {


filtro:string = '';
  respuesta: any = {
    list : []
  };


  constructor(
    private reservaService: ReservaService
  ) { }

  ngOnInit() {

    this.buscar();
  }


  buscar() {
    
    this.reservaService.getReserva().subscribe(
      (data: any) => {
        
        console.log(data);

        this.respuesta.lista = data
        
        
      },
      err => {
        
        console.log(err);
      }
    );
    console.log('agregar');
  }

}
