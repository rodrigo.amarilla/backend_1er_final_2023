import { Component, OnInit , Inject} from '@angular/core';
import { ReservaService } from 'src/services/reserva.service';
import {MatDialog, MAT_DIALOG_DATA, MatDialogRef, MatDialogModule} from '@angular/material/dialog';
import {CommonModule, NgIf} from '@angular/common';
import {MatButtonModule} from '@angular/material/button';
import {FormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';

import { BrowserModule } from '@angular/platform-browser';

import {MatSelectModule} from '@angular/material/select';


import {jsPDF} from 'jspdf';
import { elementAt } from 'rxjs';



export interface DialogData {
  animal: string;
  name: string;
}



const COLUMNS_SCHEMA = [
  
  {
      key: "id",
      type: "text",
      label: "ID"
  },{
    key: "nombre_producto",
    type: "text",
    label: "Producto"
},{
  key: "precio_venta",
  type:"text",
  label:"Precio Producto"
},
  {
      key: "cantidad",
      type: "date",
      label: "Cantidad"
  },
  {
    key: "isEdit",
    type: "isEdit",
    label: ""
  }
]

@Component({
  selector: 'app-consumo',
  templateUrl: './consumo.component.html',
  styleUrls: ['./consumo.component.css']
})
export class ConsumoComponent  implements OnInit{

  displayedColumns: string[] = COLUMNS_SCHEMA.map((col) => col.key);
  dataSource: any = [];
  columnsSchema: any = COLUMNS_SCHEMA;
  

  mesas:any = [];
  new:any={
    estado: "abierto"
  }
  cliente:any={};
  existeCliente:boolean= false;

  existeConsumo = false;

  huboError:boolean = false;
  error:any = {};
  exito:any = {};

  estados:any = [
    {
      valor:"abierto"
    },
    {
      valor:"cerrado"
    }
  ]

  constructor(
    private reservaService: ReservaService,
    public dialog: MatDialog
  ) { }
  ngOnInit(): void {
    this.getMesas();
  }

  crear() {

    this.error = {};

    if(!this.existeCliente){
      const newCliente  ={
        cedula: this.cliente.cedula,
        nombre:this.cliente.nombre,
        apellido:this.cliente.apellido
      }
      console.log(this.cliente);
      console.log(newCliente);
      this.reservaService.createCliente(newCliente).subscribe(
        (data:any)=>{
          console.log(data);
          this.new.id_cliente = data.id;
          console.log(this.new);

          var consumoTemp:any = {
            id_mesa:this.new.id_mesa,
            id_cliente:this.new.id_cliente,
            total:this.new.total,
            estado:this.new.estado
          }
          if (this.existeConsumo){
            consumoTemp.id = this.new.id;
          }
          console.log(consumoTemp);
    this.reservaService.createConsumo(consumoTemp, !this.existeConsumo).subscribe(
      (data: any) => {
        
        console.log(data);

        this.exito.message = 'exito al crear el consumo de la mesa'
        //this.mesaSeleccionada();
        
      },
      (err:any) => {
        
        console.log(err);

        this.error = err.error


      }
    );
          
        },
        (err)=> {
          console.log(err);
        }
      );
    } else {
      var consumoTemp:any = {
        id_mesa:this.new.id_mesa,
        id_cliente:this.new.id_cliente,
        total:this.new.total,
        estado:this.new.estado
      }
      if (this.existeConsumo){
        consumoTemp.id = this.new.id;
      }
      console.log(this.new);
    this.reservaService.createConsumo(consumoTemp, !this.existeConsumo).subscribe(
      (data: any) => {
        
        console.log(data);

        this.exito.message = 'exito al crear el registro'
        this.mesaSeleccionada();
        
      },
      (err:any) => {
        
        console.log(err);

        this.error = err.error


      }
    );
    }


  

    

  }


  getMesas(){
    
    this.reservaService.getMesa().subscribe(
      (data)=>{
        console.log(data);
        this.mesas = data;
      },
      (err)=>{
        console.log(err);
      }
    );
  }

  consumoEncontrado:any = {};
  mesaSeleccionada(){
    this.cliente = {};
    
    
    this.existeConsumo = false;
    this.dataSource = [];
    console.log("hacer algo");
    const id_mesa = this.new.id_mesa;
    this.new = {
      id_mesa : this.new.id_mesa
    };
    this.consumoEncontrado = {};
    this.reservaService.getConsumoByMesa(id_mesa).subscribe(
      (data:any)=>{
        console.log(data);
        if (data.length >  0 ){
          console.log("if")
          this.consumoEncontrado  = data[0];


          if(this.consumoEncontrado.estado == 'abierto'){

            
          this.new.id_cliente = this.consumoEncontrado.id_cliente;
          this.new.id_mesa = this.consumoEncontrado.id_mesa;
          this.new.total = this.consumoEncontrado.total;
          this.new.estado = this.consumoEncontrado.estado;
          this.new.id = this.consumoEncontrado.id;

          this.existeConsumo = true;
          this.new.total = 0;
            this.reservaService.getDetalleByConsumo(this.consumoEncontrado.id).subscribe(
              (data:any)=>{
                console.log(data);
                this.dataSource = data;
                console.log(this.new.total);
                data.forEach((det:any) => {
                  this.new.total = this.new.total + det.precio_venta*det.cantidad;
                  console.log(this.new.total);
                  
                });
              }
            );
            
            this.reservaService.getclienteById(this.consumoEncontrado.id_cliente).subscribe(
              (data2:any)=>{
                console.log(data2);
                const clienteTemp = data2[0];
                this.cliente.cedula = clienteTemp.cedula
                this.getCliente();
              }
            )
            
          }
        }
      },
      (err)=>{
        console.log(err)
      }
    )

  }


  getCliente() {
    this.existeCliente = false;
    
    this.reservaService.getcliente(this.cliente.cedula).subscribe(
      (data:any)=>{
        console.log(data);
        
        if(data && data.length > 0){
          console.log("entro aqqui")
          this.cliente = data[0];
          this.existeCliente = true;
          this.new.id_cliente = this.cliente.id;
        } else {
          this.cliente.nombre = "";
          this.cliente.apellido = "";
        }
      },
      (err)=>{
        console.log(err);
      }
    );
  }

  editarDetalle(element:any){
    //alert('editar consumo')
    console.log(element);
  
      
        const dialogRef = this.dialog.open(DialogCrearDetalleComponent, {
          data: {id_consumo: this.consumoEncontrado.id, 
            tipo_operacion: 'editar',
            id :element.id,
            cantidad: element.cantidad,
            id_producto:element.id_producto
          },
        });
    
        dialogRef.afterClosed().subscribe(result => {
          console.log('The dialog was closed');
          //this.animal = result;
          console.log(result);
          this.mesaSeleccionada();
        });
   
  }
  

  


  agregarDetalle(): void {
      //alert('adfas');
  
        
        const dialogRef = this.dialog.open(DialogCrearDetalleComponent, {
          data: {id_consumo: this.consumoEncontrado.id, 
            tipo_operacion: 'crear'
          },
        });
    
        dialogRef.afterClosed().subscribe(result => {
          console.log('The dialog was closed');
          //this.animal = result;
          console.log(result);
          this.mesaSeleccionada();
        });
   
    
  }

  public openPDF(): void {
    const doc = new jsPDF();
    doc.text(`Nombre Cliente: ${this.cliente.nombre}`, 10, 10);
    doc.text(`Apellido Cliente: ${this.cliente.apellido}`, 10, 20);
    doc.text(`Cedula Cliente: ${this.cliente.cedula}`, 10, 30);
    doc.text(`------------------------------`, 10, 40);
    
    doc.text(`codigo Producto`, 10, 50);
    doc.text(`producto`, 60, 50);
    doc.text(`precio unitario`, 110, 50);
    doc.text(`importe`, 160, 50);
    var posicion = 60;
    this.dataSource.forEach((element:any) => {
      console.log(element);
      doc.text(`${element.id_producto}`, 10, posicion);
      doc.text(`${element.nombre_producto} x ${element.cantidad} `, 60, posicion);
      doc.text(`${element.precio_venta}  `, 110, posicion);
      doc.text(`${element.precio_venta * element.cantidad}  `, 160, posicion);
      
      //doc.text(`${element.id_producto}  ${element.nombre_producto} x ${element.cantidad}    ${element.precio_venta * element.cantidad}`, 10, posicion);
      posicion = posicion + 10;
    });

    doc.text(`Total`, 120, posicion);
    doc.text(`${this.new.total}`, 160, posicion);

    doc.save("ticket.pdf");
  }

}


@Component({
  selector: 'app-dialog-crear-detalle',
  templateUrl: './dialog-crear-detalle.component.html',
  standalone: true,
  imports: [MatDialogModule, MatFormFieldModule, MatInputModule, FormsModule, MatButtonModule, MatSelectModule, CommonModule]
})
export class DialogCrearDetalleComponent implements OnInit{

  nuevoDetalle:any = {} ;

  productos:any= [];
  

  constructor(
    public dialogRef: MatDialogRef<DialogCrearDetalleComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private reservaService: ReservaService
  ) {}
  ngOnInit(): void {
    this.reservaService.getProducto().subscribe(
      (data:any) => {
        console.log("================================");
        
        console.log(data);
        this.productos = data;

        if(this.data.tipo_operacion == 'editar'){
          this.nuevoDetalle = {
            id:this.data.id,
            id_producto:this.data.id_producto,
            id_consumo:this.data.id_consumo,
            cantidad:this.data.cantidad
          }
          console.log(this.nuevoDetalle);
        }
        
        
      }
    )
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  crear(){
    if(this.data.tipo_operacion == 'crear'){
      this.nuevoDetalle.id_consumo = this.data.id_consumo;
      console.log(this.nuevoDetalle);
    
      this.reservaService.crearDetalle(this.nuevoDetalle).subscribe(
      data => {
        console.log(data);
        this.dialogRef.close();
      }
    );
    } else {
      
      this.nuevoDetalle.id_consumo = this.data.id_consumo;
      console.log(this.nuevoDetalle);
    
      this.reservaService.editarDetalle(this.nuevoDetalle).subscribe(
      data => {
        console.log(data);
        this.dialogRef.close();

      }
    );
    }
    
  }

  productoSeleccionado(){
    
    console.log(this.nuevoDetalle)
  }

}

