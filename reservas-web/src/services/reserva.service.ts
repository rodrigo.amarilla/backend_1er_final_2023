import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class ReservaService {

  constructor(
    private http: HttpClient,
  ) { }


  getReserva(){
    console.log(this.http);
    return this.http.get('api/cadena_restaurante/api/reserva');
  }

  getRestaurante(){
    return this.http.get('api/cadena_restaurante/api/restaurante');
  }

  getMesa(){
    return this.http.get(`api/cadena_restaurante/api/mesa`);
  }

  getMesaByRestaurante(id_restaurante:any , cantidad:any){
    return this.http.get(`api/cadena_restaurante/api/mesa?id_restaurante=${id_restaurante}&cantidad=${cantidad}`);
  }

  create(nuevo:any){

    return this.http.post(`api/cadena_restaurante/api/reserva`, nuevo);


  }

  getcliente(cedula:any){
    return this.http.get(`api/cadena_restaurante/api/cliente?cedula=${cedula}`);
  }

  getclienteById(id:any){
    return this.http.get(`api/cadena_restaurante/api/cliente?id=${id}`);
  }

  createCliente(cliente:any){
    return this.http.post(`api/cadena_restaurante/api/cliente`,cliente);
  }


  getConsumoByMesa(id_mesa:any){
    return this.http.get(`api/cadena_restaurante/api/consumo?id_mesa=${id_mesa}`);
  }

  createConsumo(consumo:any, crear:boolean){
    console.log(consumo);
    console.log(crear);
    if (crear){
      return this.http.post(`api/cadena_restaurante/api/consumo`, consumo);
    } else {
      return this.http.put(`api/cadena_restaurante/api/consumo`, consumo);
    }
    
  }

  getDetalleByConsumo(id_consumo:any){
    return this.http.get(`api/cadena_restaurante/api/detalle_consumo?id_consumo=${id_consumo}`);
  }
  getProducto(){
    return this.http.get(`api/cadena_restaurante/api/producto`);
  }

  crearDetalle(detalle:any){
    return this.http.post(`api/cadena_restaurante/api/detalle_consumo`,detalle);
  }

  editarDetalle(detalle:any){
    return this.http.put(`api/cadena_restaurante/api/detalle_consumo`,detalle);
  }
}
