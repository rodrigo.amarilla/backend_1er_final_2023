const { log } = require("console");
const db = require("../models");

const Consumo = db.Consumo;

const Op = db.Sequelize.Op;

//GET
exports.findAll = (req, res) => {

    console.log("entro aqui")

    const id_mesa = req.query.id_mesa;
    


    console.log(id_mesa);

    

    

    var condicion = {}

    if(id_mesa){
        condicion.id_mesa = id_mesa;
    }
    
    condicion.estado = 'abierto';
    console.log(condicion);
    
    

    Consumo.findAll({ 
        where: condicion ,
        order: [
            ['id','DESC']
        ]
    },)

        .then(data => {

            res.send(data);

        })

        .catch(err => {

            res.status(500).send({

                message:

                    err.message || "Ocurrio un error al obtener los categorias."

            });

        });

};

//POST
exports.create = (req,res) => {


    const consumo = {
        id_mesa: req.body.id_mesa,
        id_cliente: req.body.id_cliente,
        estado: req.body.estado,
        total: req.body.total
    }

console.log(consumo);

    
Consumo.create(consumo).then(
        (data) => {
            console.log(data);
            res.send(data);
        }
    ).catch(
        (err) => {
            res.status(500).send({

                message:

                    err.message || "Ha ocurrido un error al crear un categoria."

            });
        } 
    );


}


//PUT
exports.update = (req,res) => {


    const consumo = {
        id_mesa: req.body.id_mesa,
        id_cliente: req.body.id_cliente,
        estado: req.body.estado,
        total: req.body.total
    }

    if (consumo.estado == 'cerrado'){
        consumo.fecha_cierre = new Date();
    }

    const condicion = {
        where:{
            id:req.body.id
        }
    }
    console.log("=========================");
    console.log(consumo);
    console.log("=========================");
    Consumo.update(consumo,condicion)
    .then(
        (data) => {
            console.log(data);
            res.send(data);
        }
    ).catch(
        (err) => {
            console.log(err);
            res.status(500).send({

                message:

                    err.message || "Ha ocurrido un error al actualizar una categoria."

            });
        } 
    );
}

//DELETE
exports.destroy = (req,res) => {


        
        const condicion = {
            where:{
                id:req.body.id
            }
        }
    
    
        Categoria.destroy(condicion)
        .then(
            (data) => {
                console.log("===========================================================")
                console.log(data);
                res.send({
                    registros_eliminados: data
                });
            }
        ).catch(
            (err) => {
                res.status(500).send({
    
                    message:
    
                        err.message || "Ha ocurrido un error al borrar una mesa."
    
                });
            } 
        );
    

}



