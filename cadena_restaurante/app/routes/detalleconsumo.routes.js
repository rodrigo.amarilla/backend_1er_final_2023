module.exports = app => {

    const detalle_consumo = require("../controllers/detalle_consumodao.controller.js");

    var router = require("express").Router();

  

    
    
    router.get("/", detalle_consumo.findAll);
    router.post("/", detalle_consumo.create);
    router.put("/", detalle_consumo.update);
    router.delete("/", detalle_consumo.destroy);
    

 

    app.use('/cadena_restaurante/api/detalle_consumo', router);

};
