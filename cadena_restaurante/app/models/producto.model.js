


const db = require("../models");



module.exports = (sequelize, Sequelize) => {

    const producto = sequelize.define("Producto", {
        id:{
            type: Sequelize.BIGINT,
            primaryKey: true,
            autoIncrement: true
        },
        nombre_producto:{
            type: Sequelize.STRING,
        },
        id_categoria:{
            type: Sequelize.BIGINT,
            references:{
                model: 'Categoria',
                key:'id'
            }
        },
        precio_venta:{
            type: Sequelize.INTEGER
        }
    });

    return producto ;
}